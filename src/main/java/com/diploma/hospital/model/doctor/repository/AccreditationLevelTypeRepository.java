package com.diploma.hospital.model.doctor.repository;

import com.diploma.hospital.model.doctor.AccreditationLevelType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccreditationLevelTypeRepository extends JpaRepository<AccreditationLevelType, UUID> {
}


