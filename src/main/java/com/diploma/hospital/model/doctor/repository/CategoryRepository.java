package com.diploma.hospital.model.doctor.repository;

import com.diploma.hospital.model.doctor.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {
}
