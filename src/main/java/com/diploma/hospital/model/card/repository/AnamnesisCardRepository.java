package com.diploma.hospital.model.card.repository;


import com.diploma.hospital.model.card.AnamnesisCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AnamnesisCardRepository extends JpaRepository<AnamnesisCard, UUID> {
}
