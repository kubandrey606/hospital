package com.diploma.hospital.model.card;

import com.diploma.hospital.model.patient.Patient;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = {"patient", "outpatientRecords"})
@ToString(exclude = {"patient", "outpatientRecords"})
@Builder

@Entity
@Table
public class AnamnesisCard {

    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID anamnesisCardId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @OneToMany(mappedBy = "anamnesisCard")
    private List<OutpatientRecord> outpatientRecords;
}
