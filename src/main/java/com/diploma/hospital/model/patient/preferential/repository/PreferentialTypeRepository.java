package com.diploma.hospital.model.patient.preferential.repository;

import com.diploma.hospital.model.patient.preferential.PreferentialType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PreferentialTypeRepository extends JpaRepository<PreferentialType, UUID> {
}
