package com.diploma.hospital.model.patient.document.repository;

import com.diploma.hospital.model.patient.document.Document;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DocumentRepository extends JpaRepository<Document, UUID> {
}
