package com.diploma.hospital.model.patient.repository;

import com.diploma.hospital.model.patient.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PatientRepository extends JpaRepository<Patient, UUID> {
}
