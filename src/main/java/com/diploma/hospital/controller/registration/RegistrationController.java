package com.diploma.hospital.controller.registration;

import com.diploma.hospital.controller.registration.data.DoctorRegistrationRequest;
import com.diploma.hospital.controller.registration.data.PatientRegistrationRequest;
import com.diploma.hospital.controller.registration.service.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@AllArgsConstructor
@RestController
@RequestMapping("/registration")
public class RegistrationController {

    private final RegistrationService registrationService;


    @PostMapping("/patient")
    public void registerPatient(@RequestBody PatientRegistrationRequest request) {
        registrationService.registerPatient(request);
    }

    @PostMapping("/doctor")
    public void registerDoctor(@RequestBody DoctorRegistrationRequest request) {
        registrationService.registerDoctor(request);
    }

}
