package com.diploma.hospital.controller.doctor.service;

import com.diploma.hospital.controller.doctor.data.AddOutpatientRecordRequest;
import com.diploma.hospital.model.card.OutpatientRecord;
import com.diploma.hospital.model.card.repository.OutpatientRecordRepository;
import com.diploma.hospital.model.card.repository.OutpatientRecordTypeRepository;
import com.diploma.hospital.model.doctor.repository.DoctorRepository;
import com.diploma.hospital.model.patient.repository.PatientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@AllArgsConstructor
@Service
public class DoctorService {

    private final OutpatientRecordRepository outpatientRecordRepository;
    private final OutpatientRecordTypeRepository outpatientRecordTypeRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;


    @Transactional
    public void addOutpatientRecord(AddOutpatientRecordRequest request, UUID doctorId) {
        var record = OutpatientRecord.builder()
                .outpatientRecordId(UUID.randomUUID())
                .outpatientRecordType(outpatientRecordTypeRepository.getOne(request.getOutpatientRecordTypeId()))
                .anamnesisCard(patientRepository.getOne(request.getPatientId()).getAnamnesisCard())
                .clinicalHistory(request.getClinicalHistory())
                .diagnosis(request.getDiagnosis())
                .doctor(doctorRepository.getOne(doctorId))
                .startDate(request.getStartDate())
                .endDate(request.getEndDate())
                .prescription(request.getPrescription())
                .build();
        outpatientRecordRepository.save(record);
    }
}
