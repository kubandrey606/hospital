package com.diploma.hospital.controller.doctor.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AddOutpatientRecordRequest {
    private UUID patientId;

    private String diagnosis;

    private String clinicalHistory;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    private Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    private Date endDate;

    private UUID outpatientRecordTypeId; //32777610-1944-41f6-aa9e-44bd478792df

    private String prescription;

}
