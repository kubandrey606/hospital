package com.diploma.hospital.controller.doctor;


import com.diploma.hospital.controller.doctor.data.AddOutpatientRecordRequest;
import com.diploma.hospital.controller.doctor.service.DoctorService;
import com.diploma.hospital.model.card.OutpatientRecord;
import com.diploma.hospital.service.AnamnesisCardService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/doctor")
public class DoctorController {

    private final DoctorService doctorService;
    private final AnamnesisCardService anamnesisCardService;

    @PostMapping("/addOutpatientRecord")
    public void addOutpatientRecord(@RequestBody AddOutpatientRecordRequest request, Principal principal) {
        doctorService.addOutpatientRecord(request, UUID.fromString(principal.getName()));
    }

    @GetMapping("/card")
    public List<OutpatientRecord> getAnamnesis(@RequestParam(required = false) UUID cardId) {
        return anamnesisCardService.getRecords(cardId);
    }
}
